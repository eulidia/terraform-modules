# README #

This is a collection of Terraform Modules

### What is this repository for? ###

* Allows you to easily re-use common terraform functionality

### How do I get set up? ###

* Install Terraform (https://www.terraform.io/)
* Add a module to your terraform script