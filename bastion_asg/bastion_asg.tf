variable "ami_id" {}
variable "instance_type" {}
variable "keypair" {}

variable "aws_region" {
  default = "eu-west-1"
}

variable "availability_zones" {
  type = "list"
  default = [
    "eu-west-1a",
    "eu-west-1b",
    "eu-west-1c"
  ]
}

variable "security_groups" {
  type = "list"
  default = []
}

variable "vpc_zone_identifier" {
  type = "list"
  default = []
}

variable "min_size" {
  default = 1
}
variable "max_size" {
  default = 1
}
variable "desired_size" {
  default = 1
}

variable "name" {}

variable "health_check_grace_period" {
  default = 180
}

variable "health_check_type" {
  default = "EC2"
}

variable "Instance_Name" {}

variable "Env" {}

resource "aws_launch_configuration" "bastion_lc" {
  image_id = "${var.ami_id}"
  instance_type = "${var.instance_type}"
  key_name = "${var.keypair}"
  security_groups = ["${var.security_groups}"]
  associate_public_ip_address = true
  
  lifecycle {
      create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "bastion_asg" {
  launch_configuration = "${aws_launch_configuration.bastion_lc.id}"
  name = "${var.Env} - ${var.name}"
  max_size = "${var.min_size}"
  min_size = "${var.max_size}"
  desired_capacity = "${var.desired_size}"
  availability_zones = "${var.availability_zones}"
  vpc_zone_identifier = ["${var.vpc_zone_identifier}"]
  health_check_grace_period = "${var.health_check_grace_period}"
  health_check_type = "${var.health_check_type}"

  lifecycle {
      create_before_destroy = true
  }

  tag {
    key                 = "Name"
    value               = "${var.Instance_Name}"
    propagate_at_launch = true
  }

  tag {
    key                 = "Env"
    value               = "${var.Env}"
    propagate_at_launch = true
  }



}